package com.example.android.marsrealestate.vmc

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentVmcBinding


class VmcFragment: Fragment() {

    private val viewModel: VmcViewModel by lazy {
        ViewModelProviders.of(this).get(VmcViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentVmcBinding.inflate(inflater)
//        val binding = GridViewVmcBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel


        binding.vmcsGrid.adapter = VmcPhotoGridAdapter(VmcPhotoGridAdapter.OnClickListenerVmc{
            viewModel.displayOneVmc(it)
        })

        viewModel.navigateToSelectedVmc.observe(this, Observer {
            if ( null != it ) {
                this.findNavController().navigate(
                        VmcFragmentDirections.actionVmcFragmentToVmcDetailFragment(it))
                viewModel.displayOneVmcComplete()
            }
        })

        setHasOptionsMenu(true)
        return binding.root
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.vmc_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.updateFilter(
                when (item.itemId) {
                    R.id.show_3_axes_menu -> VmcApiFilter.AXES_3
                    R.id.show_3_plus_2_axes_menu -> VmcApiFilter.AXES_3_PLUS_2
                    R.id.show_4_axes_menu -> VmcApiFilter.AXES_4
                    R.id.show_5_axes_menu-> VmcApiFilter.AXES_5
                    else -> VmcApiFilter.AXES_ALL
                }
        )
        return true
    }
}